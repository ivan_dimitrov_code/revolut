package com.example.revolut

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.revolut.screen.ui.CurrencyFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showCurrencyScreen()
    }

    private fun showCurrencyScreen() {
        showFragmentWithTag(CurrencyFragment.TAG)
    }

    private fun showFragmentWithTag(tag: String) {
        val fragment = provideFragmentForTag(tag)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mainFragmentContainer, fragment!!, tag)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun provideFragmentForTag(tag: String): Fragment? {
        return if (supportFragmentManager.findFragmentByTag(tag) != null) {
            supportFragmentManager.findFragmentByTag(tag)
        } else {
            when (tag) {
                CurrencyFragment.TAG -> CurrencyFragment.newInstance()
                else -> Fragment()
            }
        }
    }
}
