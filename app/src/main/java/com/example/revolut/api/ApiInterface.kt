package com.example.revolut.api

import com.example.revolut.screen.model.external.ServerModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("api/android/latest")
    fun requestCrossword(
        @Query("base") baseCurrency: String
    ): Single<ServerModel>
}