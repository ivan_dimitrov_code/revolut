package com.example.revolut.screen.model.internal

data class CurrencyModel(
    val baseCurrency: String,
    val rates: Map<String, String>
)