package com.example.revolut.screen.model.external

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ServerModel(
    @SerializedName("baseCurrency")
    @Expose
    val baseCurrency: String,
    @SerializedName("rates")
    @Expose
    val rates: Map<String, String>
)