package com.example.revolut.screen.model

import com.example.revolut.screen.model.external.ServerModel
import com.example.revolut.screen.model.internal.CurrencyModel

fun adaptServerModelToAppModel(serverModel: ServerModel): CurrencyModel {
    return CurrencyModel(serverModel.baseCurrency, serverModel.rates)
}