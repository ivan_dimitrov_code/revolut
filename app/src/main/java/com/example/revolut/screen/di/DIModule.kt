package com.example.revolut.screen.di

import com.example.revolut.schedulers.BaseSchedulerProvider
import com.example.revolut.api.ApiClient
import com.example.revolut.api.ApiInterface
import com.example.revolut.screen.CurrencyViewModel
import com.example.revolut.screen.data.DataSource
import com.example.revolut.screen.data.Repository
import com.example.revolut.screen.data.remote.RemoteDataSource
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val profileViewModel = module {
    viewModel {
        CurrencyViewModel(get(), get())
    }
}

val profileRepositoryModule = module {
    fun provideRepository(remoteDataSource: DataSource): Repository {
        return Repository(remoteDataSource)
    }
    single { provideRepository(get()) }
}

val profileDataSourceModule = module {
    fun provideCurrencySourceModule(api: ApiInterface): DataSource {
        return RemoteDataSource(api)
    }
    single { provideCurrencySourceModule(get()) }
}

val apiClient = module {
    single { ApiClient.getClient() }
}

val rxScheduleProvider = module {
    fun provideScheduleProvider(): BaseSchedulerProvider {
        return SchedulerProvider.instance
    }
    single { provideScheduleProvider() }
}