package com.example.revolut.screen.data.remote

import com.example.revolut.api.ApiInterface
import com.example.revolut.screen.data.DataSource
import com.example.revolut.screen.model.adaptServerModelToAppModel
import com.example.revolut.screen.model.internal.CurrencyModel
import io.reactivex.Single

class RemoteDataSource(val api: ApiInterface) : DataSource {
    override fun provideCurrencyData(baseCurrency: String): Single<CurrencyModel> {
        return api.requestCrossword(baseCurrency).map {
            adaptServerModelToAppModel(it)
        }
    }
}
