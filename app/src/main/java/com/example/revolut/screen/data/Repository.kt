package com.example.revolut.screen.data

import com.example.revolut.screen.model.internal.CurrencyModel
import io.reactivex.Single

open class Repository(private val remoteDataSource: DataSource) {

    var cachedData: CurrencyModel? = null

    open fun provideCurrencyData(baseCurrency: String): Single<CurrencyModel> {
        return if (cachedData == null) {
            remoteDataSource.provideCurrencyData(baseCurrency).map {
                cachedData = it
                it
            }
        } else {
            Single.just(cachedData)
        }
    }

    open fun markCacheAsDirty() {
        cachedData = null
    }
}
