package com.example.revolut.screen.ui

import com.example.revolut.screen.model.internal.CurrencyModel

sealed class CurrencyUIState {
    abstract val loadedAllItems: Boolean
}

data class DefaultState(override val loadedAllItems: Boolean, val currency: CurrencyModel) : CurrencyUIState()
data class LoadingState(override val loadedAllItems: Boolean) : CurrencyUIState()
data class ErrorState(val errorMessage: String, override val loadedAllItems: Boolean) : CurrencyUIState()
