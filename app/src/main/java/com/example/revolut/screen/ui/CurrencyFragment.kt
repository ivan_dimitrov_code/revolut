package com.example.revolut.screen.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.revolut.R
import com.example.revolut.screen.CurrencyViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CurrencyFragment : Fragment() {
    private val viewModel by viewModel<CurrencyViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_currency, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        startLiveDataObservation()
    }

    private fun startLiveDataObservation() {
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            it?.let { state ->
                when (state) {
                    is DefaultState -> {
                        Log.d("data", state.currency.baseCurrency)
                    }
                    is LoadingState -> {
                        Toast.makeText(requireContext(), "Loading..", Toast.LENGTH_SHORT).show()
                    }
                    is ErrorState -> {
                        Toast.makeText(
                            requireContext(),
                            "Error ! ${state.errorMessage}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
        viewModel.requestCurrencyData("EUR")
    }

    companion object {
        const val TAG = "CurrencyFragment"
        fun newInstance() = CurrencyFragment()
    }
}
