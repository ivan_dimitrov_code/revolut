package com.example.revolut.screen

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.example.revolut.schedulers.BaseSchedulerProvider
import com.example.revolut.screen.data.Repository
import com.example.revolut.screen.ui.CurrencyUIState
import com.example.revolut.screen.ui.DefaultState
import com.example.revolut.screen.ui.ErrorState
import com.example.revolut.screen.ui.LoadingState
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class CurrencyViewModel(
    private val repository: Repository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) : ViewModel() {
    val uiState = MutableLiveData<CurrencyUIState>()
    private val disposable = CompositeDisposable()

    fun requestCurrencyData(baseCurrency: String) {
        uiState.value = LoadingState(false)

        val requestDisposable =
            Flowable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(baseSchedulerProvider.io())
                .flatMap { repository.provideCurrencyData(baseCurrency).toFlowable() }
                .observeOn(baseSchedulerProvider.ui())
                .subscribe({
                    uiState.value = DefaultState(true, it)
                }, {
                    uiState.value = it.message?.let { errorMessage ->
                        ErrorState(errorMessage, false)
                    }
                })

        disposable.add(requestDisposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun clearDisposable() {
        disposable.clear()
    }
}
