package com.example.revolut.screen.data

import com.example.revolut.screen.model.internal.CurrencyModel
import io.reactivex.Single

interface DataSource {
    fun provideCurrencyData(baseCurrency: String): Single<CurrencyModel>
}
